import { pool } from './';
import DatabaseManager from './DatabaseManager';

jest.mock('./');

describe('DatabaseManager', () => {
  describe('doesUserExist', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should return true if the user exists', async () => {
      // Mock the client returned by pool.connect()
      const mockClient = {
        query: jest.fn().mockResolvedValueOnce({ rowCount: 1 }),
        release: jest.fn(),
      };
      (pool.connect as jest.Mock).mockResolvedValueOnce(mockClient);

      const databaseManager = DatabaseManager.getInstance();
      const result = await databaseManager.doesUserExist('admin@epix.com');

      expect(pool.connect).toHaveBeenCalledTimes(1);
      expect(mockClient.query).toHaveBeenCalledWith('SELECT * FROM "Users" WHERE email = $1', [
        'admin@epix.com',
      ]);
      expect(mockClient.release).toHaveBeenCalledTimes(1);
      expect(result).toBe(true);
    });

    it('should return false if the user does not exist', async () => {
      // Mock the client returned by pool.connect()
      const mockClient = {
        query: jest.fn().mockResolvedValueOnce({ rowCount: 0 }),
        release: jest.fn(),
      };
      (pool.connect as jest.Mock).mockResolvedValueOnce(mockClient);

      const databaseManager = DatabaseManager.getInstance();
      const result = await databaseManager.doesUserExist('test@test.com');

      expect(pool.connect).toHaveBeenCalledTimes(1);
      expect(mockClient.query).toHaveBeenCalledWith('SELECT * FROM "Users" WHERE email = $1', [
        'test@test.com',
      ]);
      expect(mockClient.release).toHaveBeenCalledTimes(1);
      expect(result).toBe(false);
    });

    it('should return false if an error occurs', async () => {
      // Mock the client returned by pool.connect()
      const mockClient = {
        query: jest.fn().mockRejectedValueOnce(new Error('Database error')),
        release: jest.fn(),
      };
      (pool.connect as jest.Mock).mockResolvedValueOnce(mockClient);

      const databaseManager = DatabaseManager.getInstance();
      const result = await databaseManager.doesUserExist('test@test.com');

      expect(pool.connect).toHaveBeenCalledTimes(1);
      expect(mockClient.query).toHaveBeenCalledWith('SELECT * FROM "Users" WHERE email = $1', [
        'test@test.com',
      ]);
      expect(mockClient.release).toHaveBeenCalledTimes(1);
      expect(result).toBe(false);
    });
  });
});
