/**
 * 
 * @returns {string} - Hello World
 */
const sayHello = () => 'Hello World';

export default sayHello;
