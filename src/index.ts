import express from 'express';
import AuthRouter from './routes/AuthRouter';

/**
 * This class represents the server
 * @class
 */
class Server {
  private app: express.Application;
  private port: number;
  private authRouter: AuthRouter;

  //singleton pattern
  private static instance: Server;

  public static getInstance(): Server {
    if (!Server.instance) {
      Server.instance = new Server();
    }
    return Server.instance;
  }

  /**
   * This constructor initializes the Server object
   * @constructor
   */
  private constructor() {
    this.app = express();
    this.port = 3000;
    this.configure();
    this.setupRoutes();
    this.authRouter = new AuthRouter();
  }

  /**
   * This function configures the server
   * @returns {void}
   */
  private configure(): void {
    this.app.use(express.json());
  }

  /**
   * This function sets up the routes
   * @returns {void}
   */
  private setupRoutes(): void {
    this.app.use('/auth', this.authRouter.getRouter());
    this.app.use('/hello', (req, res) => {
      res.send('Hello World');
    });
  }

  /**
   * This function starts the server
   * @returns {void}
   */
  public start(): void {
    this.app.listen(this.port, () => {
      console.log(`Server is running on port ${this.port}`);
    });
  }
}

const server = Server.getInstance();
server.start();