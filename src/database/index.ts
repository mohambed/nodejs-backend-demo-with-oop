import { Client, Pool } from "pg";

const client = new Client({
  host: "localhost",
  port: 5432,
  user: "postgres",
  password: "1321",
  database: "OnlineBusReservation",
});

export const pool = new Pool({
  user: "postgres",
  database: "OnlineBusReservation",
  host: "localhost",
  password: "1321",
  port: 5432,
  max: 20,
  idleTimeoutMillis: 30000,
});

export default client;
