import express from 'express';
import AuthController from '../controller/authController';

/**
 * This class represents the AuthRouter
 */
class AuthRouter {
  private router = express.Router();
  private authController;

  /**
   * This constructor initializes the AuthRouter object
   * @constructor
   *
   */
  public constructor() {
    this.authController = new AuthController();
    this.router.get('/login', this.authController.login);
  }

  /**
   * This function returns the router object
   * @returns {Router} - Router object
   */
  public getRouter(): express.Router {
    return this.router;
  }
}

export default AuthRouter;
