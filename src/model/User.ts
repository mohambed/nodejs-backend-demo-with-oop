export interface IUser {
    userID: number;
    username: string;
    password: string;
    email: string;
    type: string;
    status: string;
}

class User implements IUser{
    public userID: number;
    public username: string;
    public password: string;
    public email: string;
    public type: string;
    public status: string;

    public constructor(user: IUser) {
        this.userID = user.userID;
        this.username = user.username;
        this.password = user.password;
        this.email = user.email;
        this.type = user.type;
        this.status = user.status;
    }
}

export default User;