import { Request, Response, NextFunction } from 'express';
import DatabaseManager from '../database/DatabaseManager';

class AuthController {
  private databaseManager: DatabaseManager;
  public constructor() {
    this.databaseManager = DatabaseManager.getInstance();
  }

  /**
   * This function returns the user object if the user exists in the database
   * @param req
   * @param res
   * @param next
   */
  public login = async (req: Request, res: Response, next: NextFunction) => {
    const { email, password } = req.body;
    const { user, error } = await this.databaseManager.getUser(email, password);

    if (user) {
      res.status(200).json({
        message: 'Login successful',
        user: user,
      });
    } else {
      res.status(400).json({
        message: error,
      });
    }
  };
}

export default AuthController;
