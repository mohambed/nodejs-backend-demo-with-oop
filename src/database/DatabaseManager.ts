import { pool } from './';
import User from '../model/User';

/**
 * DatabaseManager is a singleton class that handles all database queries
 */
class DatabaseManager {
  private static instance: DatabaseManager;
  private constructor() {}

  // Singleton pattern
  public static getInstance(): DatabaseManager {
    if (!DatabaseManager.instance) {
      DatabaseManager.instance = new DatabaseManager();
    }
    return DatabaseManager.instance;
  }

  /**
   * This function creates a new user
   * @param email email of the user
   * @param password password of the user
   * @returns {Promise<{user: User | null, error: string}>} - returns a user object and an error message if there is one
   */
  public async getUser(
    email: string,
    password: string
  ): Promise<{ user: User | null; error: string }> {
    const client = await pool.connect();

    try {
      const result = await client.query(
        'SELECT * FROM "Users" WHERE email = $1 AND password = $2',
        [email, password]
      );

      if (result.rowCount === 0) {
        return { user: null, error: 'Invalid email or password' };
      }
      return { user: new User(result.rows[0]), error: '' };
    } catch (err: any) {
      return { user: null, error: err.message };
    } finally {
      client.release();
    }
  }

  /**
   *
   * @param email email of the user
   * @returns {Promise<boolean>} - returns true if the user exists, false otherwise
   */
  public async doesUserExist(email: string): Promise<boolean> {
    const client = await pool.connect();

    try {
      const result = await client.query(
        'SELECT * FROM "Users" WHERE email = $1',
        [email]
      );

      if (result.rowCount === 0) {
        return false;
      }
      return true;
    } catch (err: any) {
      return false;
    } finally {
      client.release();
    }
  }

  /**
   * 
   * @returns {string} - Hello
   */
  public sayHello(): string {
    return 'Hello';
  }
}

export default DatabaseManager;
